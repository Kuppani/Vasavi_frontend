var mongoose = require('mongoose');
//var bcrypt = require('bcryptjs');

// User Schema
var BannerSchema = mongoose.Schema({
	
	banner_image: {
		type: String
	},
	category: {
		type: String
  },
  created_on: {
		type: String
  },
  status: {
		type: String
	}
});

var Banner = module.exports = mongoose.model('Banner', BannerSchema);


module.exports.saveBanner = function(postBanner, callback){
	postBanner.save(callback);
}

module.exports.getImgs = function(category, callback){
	var query = {category: category};
	Banner.find(query, callback);
}


module.exports.remove = function(id, callback){
	Banner.findByIdAndRemove({'_id' : id}, function (err,res){
		if(err) throw err;
		callback(null, res);
	  });
}


module.exports.updateBanner = function (id, res, callback) {   

	Banner.findByIdAndUpdate(id,{$set:res}, function(err, result){
			if(err) throw err;
	callback(null, result);
	});
}