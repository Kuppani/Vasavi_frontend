const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');
const User = require('../models/user');
const randomstring = require("randomstring");

//const config = require('../config/config');
//const sendMail = require('../methods/email');

// const mailgun = require('mailgun-js')({
// 	apiKey: config.API_KEY_MAILGUN,
// 	domain: config.MAILGUN_DOMAIN
// });

// The empty object '{}' passed to User crud methods is an optional options object to control the response received from database
// Right now since we are not using it, it's an empty object.

passport.serializeUser((user, done) => {
	console.log('Serializing user...'+user.id);
	done(null, user.id);
});

passport.deserializeUser((id, done) => {
	console.log('Deserializing user...');
	User.getUserById(id, function(err, user) {
		done(err, user);
	});
});

passport.use('local-signup', new LocalStrategy({
	usernameField: 'email',
	passwordField: 'password',
	passReqToCallback: true
}, (req, email, password, done) => {
	if (req.body.password !== req.body.cpassword) {
		req.flash('error', 'Passwords don\'t match!');
		req.session.save(() => {
			return done(null, false);
		});
	}else{
		var user = new User({
			username:req.body.username,
			email: req.body.email,
			password:req.body.password
		});
		console.log("User:::"+JSON.stringify(user));

		User.createUser(user, function(err, user){
			if(err){
				console.log('err: ', err);
				req.flash('error', `${err.message}`);
				req.session.save(() => {
					return done(err)
				});
			}else{
				console.log("Added user: "+user.email);
				req.flash('success', 'Registered!');
				req.session.save(() => {
					return done(null, user);
				});
			}
		});
    }

}));

passport.use('local-login', new LocalStrategy({
	usernameField: 'username',
	passwordField: 'password',
	passReqToCallback: true
}, (req, username, password, done) => {
	console.log("username::"+req.username);
	User.getUserByUsername(username, function(err, user){
		if(err) throw err;
		if(!user){
			return done(null, false, {message: 'Unknown User'});
		}

		User.comparePassword(password, user.password, function(err, isMatch){
			if(err){
				console.log('err: ', err);
				req.flash('error', `${err.message}`);
				req.session.save(() => {
					return done(err)
				});
			}
			if(isMatch){
				req.flash('success', 'Logged in successfully!');
				req.session.save(() => {
					return done(null, user);
				});
			} else {
				req.flash('error', 'Incorrect password!');
				req.session.save(() => {
					return done(null, false);
				});
			}
		});
	});
}));

module.exports = passport
