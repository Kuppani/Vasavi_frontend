var express = require('express');
var bodyParser =require('body-parser');
var path = require('path');
var expressValidator = require('express-validator');
var mongojs = require('mongojs');
var db = mongojs('customerapp',['users']);
var ObjectId = mongojs.ObjectId;

const passport = require('passport');
const session = require('express-session');
const flash = require('connect-flash');

// Mlab connection establishment
var mongoose = require('mongoose');
mongoose.connect('mongodb://pravali:rgu2012@ds245337.mlab.com:45337/sample_jwt');

const hour = 3600000;
var app = express();

/*
var logger = function(req,res,next){
	console.log('logging');
	next();
}
*/

// View Engine
app.set('view engine','ejs');
app.set('views',path.join(__dirname,'views'));

//Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

// Set Static Path
app.use(express.static(path.join(__dirname,'public')));
/*
var person = {
	name:'sasi',
	age:23
}
*/
//Flash messages
app.use(flash());

// //global variables
// app.use(function(req,res,next){
// 	res.locals.errors = null;
// 	res.locals.users = null;
// 	next();
// });

app.use(session({ //will work with everything below this so declare your static assets above
  secret: 'This is not a very good secr3t',
  resave: false,
  saveUninitialized: false,
  cookie: {
      httpOnly: true, //only allows cookie access to server
      maxAge: hour
  }
}));
//passport config
app.use(passport.initialize());
app.use(passport.session()); //At this point of time flash messages are not working; Have to revisit this later for debugging

//Custom Middleware
app.use((req, res, next) => {
  res.locals.currentUser = (req.user) ? req.user.username : undefined;
  res.locals.success = req.flash('success');
  res.locals.error = req.flash('error');
  console.log('\nThe locals object is\n',res.locals);
  next();
});


// Express validator middleware
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));

//Routings
var index = require('./routes/index');
var authenticate=require('./routes/authenticate');
app.use("/",index);
app.use("/",authenticate);

app.listen(3000,function(){
	console.log('server started on port 3000');
})
