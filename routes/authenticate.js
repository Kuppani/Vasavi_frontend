const router = require('express').Router();

const crypto = require('crypto');
const cryptoAlgorithm = 'aes-256-ctr';
const cryptoPassword = 'keerosResetPassword';

const bcrypt = require('bcryptjs');
const randomstring = require("randomstring");

//const config = require('../config/config');
const User = require('../models/user');

//const sendMail = require('../methods/email');
//const encrypt = require('../methods/encrypt');

const passport = require('../config/passport');
const middleware = require('../middleware/index');

// const mailgun = require('mailgun-js')({
// 	apiKey: config.API_KEY_MAILGUN,
// 	domain: config.MAILGUN_DOMAIN
// });

const cryptoEncrypt = (text) => {
	const cipher = crypto.createCipher(cryptoAlgorithm, cryptoPassword);
	let crypted = cipher.update(text, 'utf8', 'hex');
	crypted += cipher.final('hex');
	return crypted;
}

const cryptoDecrypt = (text) => {
	const decipher = crypto.createDecipher(cryptoAlgorithm, cryptoPassword);
	let decrypted = decipher.update(text, 'hex', 'utf8');
	decrypted += decipher.final('utf8');
	return decrypted;
}

router.post('/signup', (req, res, next) => {
	passport.authenticate("local-signup", {
		successRedirect: '/',
		failureRedirect: '/login',
		successFlash: true,
		failureFlash: true,
	})(req, res, next)
}, (req, res) => {});

router.post('/login',
	passport.authenticate("local-login", {
		successRedirect: '/',
		failureRedirect: '/login',
		successFlash: true,
		failureFlash: true,
	}), (req, res) => {});

router.get('/logout', (req, res) => {
	req.logOut();
	req.flash('success', 'Successfully Logged out!');
	//customRedirect(req, res);
	res.redirect("/");
});

//Signup Verification
router.get('/activateaccount', (req, res) => {
	const queryParams = req.query; // query_string = {emailid,token}
	const userEmail = queryParams.emailid;
	console.log('queryParams: ', queryParams);
	User.selectTable("pravallika_user_data");
	User.getItem({
		email: userEmail
	}, {}, (err, user) => {
		console.log('email checking');
		if (err) {
			console.log('\nerr', err);
			req.flash('error', 'Invalid token!');
			customRedirect(req, res);
		} else if (Object.keys(user).length === 0) {
			console.log('\nNo user');
			req.flash('error', `Verification Failed!`);
			customRedirect(req, res);
		} else if (Object.keys(user).length > 0) {
			console.log('In Signup verification');
			console.log(user.verification_code, queryParams.token);
			if (user.verifiedornot === 'yes') {
				// res.send(statusMsg.errorResponse('Already verified!'));
				req.flash('error', 'Already verified!');
				customRedirect(req, res);
			} else if (user.verification_code === queryParams.token) {
				const updateParams = {
					email: userEmail,
					verifiedornot: 'yes'
				}
				console.log("Updating the item...");
				User.updateItem(updateParams, {}, (err, data) => {
					if (err) {
						console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
						req.flash('error', 'Error verifying')
						customRedirect(req, res);
						// res.send(statusMsg.errorResponse(err));
					} else {
						console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
						// res.send(statusMsg.verifySuccess)
						req.flash('success', 'Verified successfully!');
						customRedirect(req, res);
					}
				});
			} else {
				req.flash('error', 'Invalid token!');
				customRedirect(req, res);
			}
		}
	})
});

router.get('/forgotpassword', (req, res) => {
	//Enter your emailid to submit forgot password request
	res.render('forgotpassword');
});

// Forgot Password
router.post('/forgotpassword', (req, res) => {
	//Received a forgotpassword request from a mailid.
	const userEmail = req.body.email;
	User.selectTable("pravallika_user_data");
	User.getItem({
		email: userEmail
	}, {}, (err, user) => {
		if (err) {
			req.flash('error', `${err.message}`);
			customRedirect(req, res);
		} else if (Object.keys(user).length) {
			console.log('user: ', user);
			//user.email is the emailid of the user received from the database after
			// verifying if there's a userid submitted from the forgot password form
			const encryptedMail = cryptoEncrypt(user.email);
			//This can be replaced with the DNS name of the server later;
			const rootUrl = `${req.protocol}://${req.get('host')}`;
			const dataToSend = sendMail.forgotPasswordMail(user.email, encryptedMail, rootUrl);
			mailgun.messages().send(dataToSend, (err, body) => {
				if (err) {
					req.flash('error', `${err.message}`);
					customRedirect(req, res);
				} else {
					console.log("email sent");
					req.flash('success', 'Email sent to your maild!');
					customRedirect(req, res);
				}
			});
		} else {
			req.flash('error', `No user found with that email`);
			customRedirect(req, res);
		}
	})

});

// Update Password
router.post('/updatepassword', middleware.isLoggedIn, (req, res) => {
	const bodyParams = req.body;
	console.log('\nbodyParams:\n', bodyParams);
	console.log('\nreq User\n', req.user);
	const userEmail = req.user.email;
	const hashPassword = req.user.password;

	if (bodyParams.newpassword !== bodyParams.cnewpassword) {
		req.flash('error', 'New password should match re-enter password');
		customRedirect(req, res, 'profile');
	} else if (bodyParams.newpassword.length < 6) {
		req.flash('error', 'Password should contain atleast 6 characters');
		customRedirect(req, res, 'profile');
	} else {
		//compare the entered password with hashed password
		bcrypt.compare(bodyParams.password, hashPassword, (err, result) => {
			if (err) {
				console.log('\nUpdate password err\n', err);
				req.flash('error', err.message);
				customRedirect(req, res, 'profile');
			} else if (result) { //if passwords match
				const updateCallback = (hashnewpwd) => {
					const updateParams = {
						email: userEmail,
						password: hashnewpwd
					};
					console.log("\nUpdating the item...\n");
					User.selectTable("pravallika_user_data");
					User.updateItem(updateParams, {}, (err, data) => {
						if (err) {
							console.error("\nUnable to update item\n", err);
							req.flash('error', err.message);
							customRedirect(req, res, 'profile');
						} else {
							console.log("\nUpdateItem succeeded\n", data);
							req.flash('success', 'Password updated! Please login again');
							customRedirect(req, res, 'logout');
						}
					});
				};
				encrypt.generateSalt(res, bodyParams.newpassword, updateCallback);
			} else { //if passwords don't match
				console.log('\npasswords don\'t match\n');
				req.flash('error', 'Passwords don\'t match');
				customRedirect(req, res, 'profile');
			}
			//Called after the above if-else
		});
	} //else end


});

// Reset password form 
router.get('/resetpassword', (req, res) => {

	const queryParams = req.query;
	console.log('queryParams: ', queryParams);
	//Check the queryparams to get the correct query string parameter
	const encryptedMail = queryParams.token || ''; //get encryptedemail from the query string

	const decryptedMail = cryptoDecrypt(encryptedMail);
	User.selectTable("pravallika_user_data");
	User.getItem({
		email: decryptedMail
	}, {}, (err, user) => {
		console.log('email checking', Object.keys(user).length);
		if (err) {
			req.flash('error', `${err.message}`);
			customRedirect(req, res);
		} else if (Object.keys(user).length === 0) {
			req.flash('error', `Incorrect user`);
			customRedirect(req, res);
		} else {
			res.locals.email = decryptedMail;
			res.render('resetpassword');
		}
	});

});

//
router.post('/resetpassword', (req, res) => {

	console.log('req.body: ', req.body);
	const bodyParams = req.body;
	const userEmail = bodyParams.email;
	const newpassword = bodyParams.newpassword

	User.selectTable("pravallika_user_data");
	User.getItem({
		email: userEmail
	}, {}, (err, user) => {
		console.log('email checking', Object.keys(user).length);
		if (err) {
			req.flash('error', `${err.message}`);
			customRedirect(req, res);
		} else if (Object.keys(user).length === 0) {
			req.flash('error', `Incorrect user`);
			customRedirect(req, res);
		} else {
			const updateCallback = (hashnewpwd) => {
				const updateParams = {
					email: userEmail,
					password: hashnewpwd
				};
				User.updateItem(updateParams, {}, (err, data) => {
					if (err) {
						req.flash('error', `${err.message}`);
						customRedirect(req, res);
					} else {
						req.flash('success', 'Reset password successfully!');
						customRedirect(req, res);
					}
				});
			};
	
			encrypt.generateSalt(res, newpassword, updateCallback);
		}

	});
});

module.exports = router;
