var express = require('express');
var router = express.Router();
var Banner = require('../models/banner');


router.get('/', function(req, res){
	Banner.getImgs("home",function(err,imgs){
		if(err){
			throw err;
		}else{
			//console.log("images"+JSON.stringify(imgs));
			res.locals.images=imgs;
			res.render('index',{
				act_state : 'home'
			});
		}
	});
});

router.get('/aboutus',function(req,res){
	Banner.getImgs("home",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('aboutus',{
				act_state : 'aboutus'
			});
		}
	});
});

router.get('/academicprograms',function(req,res){
	Banner.getImgs("home",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('academicprograms',{
				act_state : 'academicprograms'
			});
		}
	});
});

router.get('/admission',function(req,res){
	Banner.getImgs("home",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('admission',{
				act_state : 'admission'
			});
		}
	});
});

router.get('/gallery',function(req,res){
	Banner.getImgs("home",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('gallery',{
				act_state : 'gallery'
			});
		}
	});
});

router.get('/resources',function(req,res){
	Banner.getImgs("home",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('resources',{
				act_state : 'resources'
			});
		}
	});
});

router.get('/careers',function(req,res){
	Banner.getImgs("home",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('careers',{
				act_state : 'careers'
			});
		}
	});
});

router.get('/login',function(req,res){
	res.render('login',{
		act_state : 'login'
	});
});

router.get('/iris-home',function(req,res){
	Banner.getImgs("iris",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('iris-index',{
				act_state : 'iris-index'
			});
		}
	});
});

router.get('/iris-scholastic',function(req,res){
	Banner.getImgs("iris",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('iris-scholastic',{
				act_state : 'iris-scholastic'
			});
		}
	});
});

router.get('/iris-co-scholastic',function(req,res){
	Banner.getImgs("iris",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('iris-co-scholastic',{
				act_state : 'iris-co-scholastic'
			});
		}
	});
});

router.get('/iris-facilities',function(req,res){
	Banner.getImgs("iris",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('iris-facilities',{
				act_state : 'iris-facilities'
			});
		}
	});
});

router.get('/primary-home',function(req,res){
	Banner.getImgs("primary",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('primary-index',{
				act_state : 'primary-index'
			});
		}
	});
});

router.get('/primary-scholastic',function(req,res){
	Banner.getImgs("primary",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('primary-scholastic',{
				act_state : 'primary-scholastic'
			});
		}
	});
});

router.get('/primary-co-scholastic',function(req,res){
	Banner.getImgs("primary",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('primary-co-scholastic',{
				act_state : 'primary-co-scholastic'
			});
		}
	});
});

router.get('/primary-facilities',function(req,res){
	Banner.getImgs("primary",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('primary-facilities',{
				act_state : 'primary-facilities'
			});
		}
	});
});

router.get('/primary-achievements',function(req,res){
	Banner.getImgs("primary",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('primary-achievements',{
				act_state : 'primary-achievements'
			});
		}
	});
});

router.get('/primary-creative-corner',function(req,res){
	Banner.getImgs("primary",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('primary-creative-corner',{
				act_state : 'primary-creative-corner'
			});
		}
	});
});

router.get('/primary-parent-portal',function(req,res){
	Banner.getImgs("primary",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('primary-parent-portal',{
				act_state : 'primary-parent-portal'
			});
		}
	});
});

router.get('/primary-resources',function(req,res){
	Banner.getImgs("primary",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('primary-resources',{
				act_state : 'primary-resources'
			});
		}
	});
});

router.get('/highschool-home',function(req,res){
	Banner.getImgs("high_school",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('highschool-index',{
				act_state : 'highschool-home'
			});
		}
	});
});

router.get('/highschool-scholastic',function(req,res){
	Banner.getImgs("high_school",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('highschool-scholastic',{
				act_state : 'highschool-scholastic'
			});
		}
	});
});

router.get('/highschool-co-scholastic',function(req,res){
	Banner.getImgs("high_school",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('highschool-co-scholastic',{
				act_state : 'highschool-co-scholastic'
			});
		}
	});
});

router.get('/highschool-facilities',function(req,res){
	Banner.getImgs("high_school",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('highschool-facilities',{
				act_state : 'highschool-facilities'
			});
		}
	});
});

router.get('/highschool-achievements',function(req,res){
	Banner.getImgs("high_school",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('highschool-achievements',{
				act_state : 'highschool-achievements'
			});
		}
	});
});

router.get('/highschool-parent-portal',function(req,res){
	Banner.getImgs("high_school",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('highschool-parent-portal',{
				act_state : 'highschool-parent-portal'
			});
		}
	});
});

router.get('/highschool-resources',function(req,res){
	Banner.getImgs("high_school",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('highschool-resources',{
				act_state : 'highschool-resources'
			});
		}
	});
});

router.get('/icon-home',function(req,res){
	Banner.getImgs("icon",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('icon-index',{
				act_state : 'icon-home'
			});
		}
	});	
});

router.get('/icon-scholastic',function(req,res){
	Banner.getImgs("icon",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('icon-scholastic',{
				act_state : 'icon-scholastic'
			});
		}
	});	
});

router.get('/icon-co-scholastic',function(req,res){
	Banner.getImgs("icon",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('icon-co-scholastic',{
				act_state : 'icon-co-scholastic'
			});
		}
	});	
});

router.get('/icon-facilities',function(req,res){
	Banner.getImgs("icon",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('icon-facilities',{
				act_state : 'icon-facilities'
			});
		}
	});	
});

router.get('/icon-achievements',function(req,res){
	Banner.getImgs("icon",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('icon-achievements',{
				act_state : 'icon-achievements'
			});
		}
	});	
});

router.get('/icon-parent-portal',function(req,res){
	Banner.getImgs("icon",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('icon-parent-portal',{
				act_state : 'icon-parent-portal'
			});
		}
	});	
});

router.get('/icon-resources',function(req,res){
	Banner.getImgs("icon",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('icon-resources',{
				act_state : 'icon-resources'
			});
		}
	});	
});

router.get('/reachus',function(req,res){
	Banner.getImgs("icon",function(err,imgs){
		if(err){
			throw err;
		}else{
			res.locals.images=imgs;
			res.render('reachus',{
				act_state : 'reachus'
			});
		}
	});	
});

module.exports = router;